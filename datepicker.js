import React, { Component } from "react";

import { Form, DatePicker } from "antd";

export class Test extends Component {
  state = { dem_date: "", other_date: "" };

  handleDate = (dateString, id) => {
    this.setState({ [id]: dateString });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dem_date, other_date } = this.state;
    const date = {
      dem_date: dem_date,
      other_date: other_date
    };
    console.log(date);
  };

  render() {
    return (
      <div>
        <div>
          <Form onSubmit={this.handleSubmit}>
            {/* First Name */}

            <Form.Item>
              <DatePicker
                onChange={(date, dateString) =>
                  this.handleDate(dateString, "dem_date")
                }
              />
            </Form.Item>
            <Form.Item>
              <DatePicker
                onChange={(date, dateString) =>
                  this.handleDate(dateString, "other_date")
                }
              />
            </Form.Item>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </Form>
        </div>
      </div>
    );
  }
}

export default Test;

